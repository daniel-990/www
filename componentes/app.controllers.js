(function(){
	'use strict';
	angular
		.module('app')
		.controller('File1Controller', File1Controller)
		.controller('File2Controller', File2Controller)
		.controller('File3Controller', File3Controller);

///Users/daniel8bit/Library/Android/sdk

        var config = {
            apiKey: "AIzaSyAA2oobvOeDZUEx-wzj7FJlL4CZIvv0v64",
            authDomain: "danielarango990-29a5e.firebaseapp.com",
            databaseURL: "https://danielarango990-29a5e.firebaseio.com",
            storageBucket: "danielarango990-29a5e.appspot.com",
            messagingSenderId: "887049675040"
        };
        firebase.initializeApp(config);
        var recuperar_mensajes = firebase.database().ref('mensajes');

        $("#cerrar-sesion").on('click', function(){
            firebase.auth().signOut().then(function() {
                    var urlDominio = window.location.host;
                    window.location.href = 'http://'+urlDominio+"/#/file1";
            },function(error) {
                alert("error! ", error);
            });

        })
     
     function File1Controller($stateParams, $http){
            var vm = this;
            $http.get('../json/data.json')
            .then(function(res){
                vm.dataP = res.data; 
                var json = vm.rutasMed = res.data; 
            });


        $("#recuperar").on('click', function(){
            var auth = firebase.auth();
            var emailAddress = prompt('Ingrese su correo para recuperar contraseña: ');
            auth.sendPasswordResetEmail(emailAddress).then(function() {
            }, function(error) {
                alert("error! ",error);
            });
        })

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                    var urlDominio = window.location.host;
                    window.location.href = "#/file2";
            } else {
                //...
            }
        });


        $("#entrar").on('click', function(){
            var correo = $("#correo").val();
            var pass = $("#pass").val();

            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    var urlName = window.location.pathname;
                    var urlDominio = window.location.host;
                    window.location.href = "#/file2";
                } else {
                    var correo = $("#correo").val("");
                    var pass = $("#pass").val(""); 

                    Materialize.toast('Datos incorrectos..', 500);
                }
            });
            firebase.auth().signInWithEmailAndPassword(correo, pass);
        })
     }

     function File2Controller($stateParams){
 		var vm = this;
        $(".button-collapse").sideNav();
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                    var plantilla_usuario = $("#plantilla_usuario").html();
                    var templateUsuario = Handlebars.compile(plantilla_usuario);
                    var contexto_usuario = {
                        nombre_usuario: user.email
                    }
                    var compilador_usuario = templateUsuario(contexto_usuario);
                    $('#usuario-name').html(compilador_usuario);
                    verDatos();
            } else {
                var urlDominio = window.location.host;
                window.location.href = "#/file1";

                Materialize.toast('Tiene que estar registrado para continuar', 500);
            }
        });

        function verDatos(){
            recuperar_mensajes.on('child_added', function(data){

                var plantilla_datos = $("#plantilla_datos").html();
                var templateDatos = Handlebars.compile(plantilla_datos);

                var contexto_datos = {
                    nombre_render: data.val().nombre,
                    correo_render: data.val().correo,
                    mensaje_render: data.val().mensaje
                }
                var compilador_datos = templateDatos(contexto_datos);
                $('#render-datos').append(compilador_datos);
            });
        }

     }

     function File3Controller($stateParams){
			var vm = this;

            firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                //...
            } else {

                var urlDominio = window.location.host;
                window.location.href = 'http://'+urlDominio+"/#/file1";

                Materialize.toast('Tiene que estar registrado para continuar', 500);

            }
        });
     }

})();
